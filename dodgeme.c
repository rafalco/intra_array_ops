/* Trivial program to dodge a specified element of an array and print the others.
   Copyright (C) Ramón Fallon

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if(argc!=3) {
        printf("Error. Pls supply 2 arguments, 1) size of array and 2) index of element you want skipped.\n");
        exit(EXIT_FAILURE);
    }
    int i=atoi(argv[2]), j, m, n=atoi(argv[1]);

    printf("%i: ", i);  /* just checking */
    for(j=0;j<n-1;++j) {
        m=(i+j+1)%n; /* thi is the relational index, calculated on the basis of the operative one (see above) which will always dodge it and go everywhere else */
        printf("%i ", m);
    }
    printf("\n"); 
    return 0;
}
